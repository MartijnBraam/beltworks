#!/bin/sh

BASE="belt_corner_base.png"
OVERLAY="belt_corner_overlay.png"
MASK="belt_corner_mask.png"

OFFSET=0
FRAMES=16

while [ $OFFSET -lt $FRAMES ]; do
	echo "FRAME $OFFSET"

	FRAMENAME="$(printf "/tmp/frame-%02d" $OFFSET).png"

	# Shift frame
	convert $OVERLAY -roll "+0-${OFFSET}" "rolled-a.png"

	# Rotate
	convert rolled-a.png -rotate 90 rolled-b.png

	# Extract alpha
	convert rolled-a.png -alpha extract alpha-a.png
	convert rolled-b.png -alpha extract alpha-b.png

	# Mask off half
	convert alpha-a.png -fill black -draw "polyline 0,0 16,0 16,16"  alphamask-a.png
	convert alpha-b.png -fill black -draw "polyline 0,1 0,16 15,16" alphamask-b.png

	# Apply new alpha mask
	convert rolled-a.png alphamask-a.png -alpha Off -compose CopyOpacity -composite masked-a.png
	convert rolled-b.png alphamask-b.png -alpha Off -compose CopyOpacity -composite masked-b.png

	# Create composite
	composite masked-a.png $BASE composite-a.png
	composite masked-b.png composite-a.png "${FRAMENAME}.png"

	OFFSET=$(($OFFSET + 1))
done

montage /tmp/frame-*.png -tile 1x16 -geometry 16x16+0+0 "beltworks_belt_corner_anim.png"

rm -f rolled-a.png rolled-b.png alpha-a.png alpha-b.png alphamask-a.png alphamask-b.png masked-a.png masked-b.png composite-a.png
