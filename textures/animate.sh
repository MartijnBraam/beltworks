#!/bin/sh

OFFSET=0
FRAMES=16

while [ $OFFSET -lt $FRAMES ]; do
	echo "FRAME $OFFSET"
	convert "$1" -roll "+0-${OFFSET}" "$(printf "/tmp/frame-%02d.png" $OFFSET)"
	OFFSET=$(($OFFSET + 1))
done

montage /tmp/frame-*.png -tile 1x16 -geometry 16x16+0+0 "$2"
