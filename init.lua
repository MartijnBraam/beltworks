-- Beltworks by MartijnBraam
--

beltworks = {}

local DEBUG = false

beltworks.worldpath = minetest.get_worldpath()
beltworks.modpath = minetest.get_modpath("beltworks")

dofile(beltworks.modpath.."/belts.lua")
dofile(beltworks.modpath.."/inserters.lua")

print("Beltworks loaded!")
