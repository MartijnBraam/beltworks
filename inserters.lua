minetest.register_node("beltworks:inserter", {
    description = "Inserter",
	drawtype = "nodebox",
	sunlight_propagates = true,

	node_box = {
		type = "fixed",
		fixed = {
			{-0.5+0.0625, -0.5,-0.25 , -0.25, 0.5, 0.25},
			{0.5-0.0625, -0.5, -0.25,  0.25, 0.5, 0.25},
		},
	},
	tiles = {
		"beltworks_inserter_front.png",
		"beltworks_inserter_front.png",
		"beltworks_inserter_side.png",
		"beltworks_inserter_side2.png",
		"beltworks_inserter_front.png",
		"beltworks_inserter_front.png",
	},
	paramtype = "light",
	paramtype2 = "facedir",

	sounds = default.node_sound_metal_defaults(),
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2},

	on_place = function(itemstack, placer, pointed_thing)
		pos = pointed_thing.above
		pos.y = pos.y + 0.25
		local obj = minetest.add_entity(pos, "beltworks:inserter_arm")
		return minetest.rotate_node(itemstack, placer, pointed_thing)
	end
})

minetest.register_entity("beltworks:inserter_arm", {
	initial_properties = {
		hp_max = 1,
		physical = true,
		collide_with_objects = false,
		collisionbox = {-0.3, -0.3, -0.3, 0.3, 0.3, 0.3},
		visual = "mesh",
		mesh = "beltworks_inserter_arm.obj",
		visual_size = {x = 1, z = 1, y = 1},
		textures = {"beltworks_inserter_arm.png"},
		spritediv = {x = 1, y = 1},
		initial_sprite_basepos = {x = 0, y = 0},
	},
})