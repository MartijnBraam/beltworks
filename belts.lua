minetest.register_abm({
	nodenames = {"beltworks:belt"},
	neighbors = nil,
	interval = 1,
	chance = 1,
	action = function(pos, node, active_object_count, active_object_count_wider)
		local all_objects = minetest.get_objects_inside_radius(pos, 0.75)
		local _,obj
		for _,obj in ipairs(all_objects) do
			print(obj)
			if not obj:is_player() and obj:get_luaentity() and obj:get_luaentity().name == "__builtin:item" then
				beltworks.do_moving_item({x = pos.x, y = pos.y + 0.15, z = pos.z}, obj:get_luaentity().itemstring)
				obj:get_luaentity().itemstring = ""
				obj:remove()
			end
		end
	end,
})

function beltworks.do_moving_item(pos, item)
	local stack = ItemStack(item)

	-- TODO: Snap new item to the left or right side of the belt
	local obj = minetest.add_entity(pos, "beltworks:moving_item")
	obj:get_luaentity():set_item(stack:to_string())
	return obj
end

minetest.register_entity("beltworks:moving_item", {
	initial_properties = {
		hp_max = 1,
		physical = false,
		collisionbox = {0.125, 0.125, 0.125, 0.125, 0.125, 0.125},
		visual = "wielditem",
		visual_size = {x = 0.2, y = 0.2},
		textures = {""},
		spritediv = {x = 1, y = 1},
		initial_sprite_basepos = {x = 0, y = 0},
		is_visible = false,
	},

	physical_state = true,
	itemstring = '',
        current_belt = nil,

	set_item = function(self, itemstring)
		print("LANDED "..itemstring)
		self.itemstring = itemstring
		local stack = ItemStack(itemstring)
		local s = 0.15
		local c = 0.8 * s
		local itemtable = stack:to_table()
		local itemname = nil
		if itemtable then
			itemname = stack:to_table().name
		end
		local item_texture = nil
		local item_type = ""
		if core.registered_items[itemname] then
			item_texture = core.registered_items[itemname].inventory_image
			item_type = core.registered_items[itemname].type
		end
		prop = {
			is_visible = true,
			visual = "wielditem",
			textures = {itemname},
			visual_size = {x = s, y = s},
			collisionbox = {-c, -c, -c, c, c, c},
			automatic_rotate = 0,
		}
		self.object:set_properties(prop)
		local pos = self.object:getpos()

		-- Normalize height
		pos.y = math.floor(pos.y)

		-- Snap to left or right side of the belt
		local napos = minetest.get_node(pos)
		local center = 0
		if napos.param2 == 0 or napos.param2 == 2 then
			center = math.floor(pos.x+0.5)
			print(pos.x.." > "..center)
			if pos.x > center-0.5 then
				pos.x = center + 0.165
			else
				pos.x = center - 0.165
			end
		else
			center = math.floor(pos.z+0.5)
			if pos.z > center-0.5 then
				pos.z = center + 0.165
			else
				pos.z = center - 0.165
			end
		end

		self.object:setpos(pos)
		current_belt = minetest.get_node(pos) 
	end,

	get_staticdata = function(self)
		return core.serialize({
			itemstring = self.itemstring
		})
	end,

	on_activate = function(self, staticdata, dtime_s)
		if string.sub(staticdata, 1, string.len("return")) == "return" then
			local data = core.deserialize(staticdata)
			if data and type(data) == "table" then
				self.itemstring = data.itemstring
			end
		else
			self.itemstring = staticdata
		end
		self.object:set_armor_groups({immortal = 1})
		if self.itemstring ~= "" then
			self:set_item(self.itemstring)
		end
	end,

	on_step = function(self, dtime)
		local pos = self.object:getpos()
		local stack = ItemStack(self.itemstring)
		local napos = minetest.get_node(pos)

		local veldir = self.object:getvelocity();

		if napos.name == "beltworks:belt" then
			if napos ~= current_belt then

				-- Center on one of the sides of the new belt
				local center = 0
				if napos.param2 == 0 or napos.param2 == 2 then

					center = math.floor(pos.x+0.5)

					if pos.x > center then

						pos.x = center + 0.165
					else

						pos.x = center - 0.165
					end
				else
					center = math.floor(pos.z+0.5)
					if pos.z > center then
						pos.z = center + 0.165
					else
						pos.z = center - 0.165
					end
				end
				self.object:move_to(pos)

				if napos.param2 ~= current_belt.param2 then
					-- Moved to a new belt with another direction, but not with a corner belt so this is a T connection.
					print("New direction: "..napos.param2)

					-- TODO: Store belt capacity in node metadata so belts can block when full
					-- TODO: Move item to closest side of the belt if its a 90 degree angle
					-- move_to() should do an interpolated move

				end
				current_belt = napos
			end

			local speed = 1
			local dir = minetest.facedir_to_dir(napos.param2)
			self.object:setvelocity({x = dir.x / speed, y = 0, z = dir.z / speed})
		elseif napos.name == "beltworks:belt_cw" then

			local speed = 1

			-- Treat the belt as a sideways belt, except for one quarter where it moves in the input direction
			-- This makes sure that the items keep on the same side of the belt on the input and output

			local dir_output = minetest.facedir_to_dir((napos.param2+1)%4)
			local dir_input = minetest.facedir_to_dir(napos.param2)
			local dir = nil
			local center = 0

			if napos.param2 == 1 then
				center_out = math.floor(pos.x+0.5)
				center_in  = math.floor(pos.z+0.5)
				if pos.z > center_in and pos.x < center_out then
					dir = dir_input
				else
					dir = dir_output
					if pos.x < center_out then
						pos.x = center_out - 0.165
					else
						pos.x = center_out + 0.165
					end
				end
			elseif napos.param2 == 2 then
				center_out = math.floor(pos.z+0.5)
				center_in  = math.floor(pos.x+0.5)
				if pos.x > center_in and pos.z > center_out then
					dir = dir_input
				else
					dir = dir_output
					if pos.z < center_out then
						pos.z = center_out - 0.165
					else
						pos.z = center_out + 0.165
					end
				end
			elseif napos.param2 == 3 then
				center_out = math.floor(pos.x+0.5)
				center_in  = math.floor(pos.z+0.5)
				if pos.z < center_in and pos.x > center_out then
					dir = dir_input
				else
					dir = dir_output
					if pos.x < center_out then
						pos.x = center_out - 0.165
					else
						pos.x = center_out + 0.165
					end
				end
			elseif napos.param2 == 0 then
				center_out = math.floor(pos.z+0.5)
				center_in  = math.floor(pos.x+0.5)
				if pos.x < center_in and pos.z < center_out then
					dir = dir_input
				else
					dir = dir_output
					if pos.z < center_out then
						pos.z = center_out - 0.165
					else
						pos.z = center_out + 0.165
					end
				end
			end

			self.object:move_to(pos)
			current_belt = napos
			self.object:setvelocity({x = dir.x / speed, y = 0, z = dir.z / speed})
		else
			minetest.add_item( {x = pos.x + veldir.x / 3, y = pos.y, z = pos.z + veldir.z / 3}, stack)
			self.object:remove()
		end
	end
})



minetest.register_node("beltworks:belt", {
    description = "Belt",
	drawtype = "nodebox",
	sunlight_propagates = true,

	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.25, 0.5},
		},
	},
	paramtype = "light",
	paramtype2 = "facedir",
	
	sounds = default.node_sound_metal_defaults(),

	tiles = {
		{
			name = "beltworks_belt_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0
			}
		},
		{
			name = "beltworks_belt_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0
			}
		},
		"beltworks_belt_side.png",
		"beltworks_belt_side.png",
		{
			name = "beltworks_belt_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0
			}
		},
		{
			name = "beltworks_belt_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1.0
			}
		},
	},
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2},

	on_place = minetest.rotate_node,
})


minetest.register_node("beltworks:belt_cw", {
    description = "Belt Clockwise corner",
	drawtype = "nodebox",
	sunlight_propagates = true,

	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.25, 0.5},
		},
	},
	paramtype = "light",
	paramtype2 = "facedir",

	sounds = default.node_sound_metal_defaults(),

	tiles = {
		{
			name = "beltworks_belt_corner_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5
			}
		},
		{
			name = "beltworks_belt_anim.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 0.5
			}
		},
		"beltworks_belt_side.png",
		"beltworks_belt_side.png",
		"beltworks_belt_side.png",
		"beltworks_belt_side.png",
	},
	groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2},

	on_place = minetest.rotate_node,
})


minetest.register_craft({
	output = 'beltworks:belt 3',
	recipe = {
		{'', '', ''},
		{'', '', ''},
		{'default:steel_ingot', 'default:steel_ingot', 'default:steel_ingot'},
	}
})

minetest.register_craft({
	output = 'beltworks:belt_cw 3',
	recipe = {
		{'beltworks:belt', 'beltworks:belt', ''},
		{'beltworks:belt', '', ''},
		{'', '', ''},
	}
})